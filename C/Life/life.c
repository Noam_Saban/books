#include "life.h"

void clear()
{
    int i;
    printf("\n");
    for(i=0;i<SIZE * 2;i++)
    {
        printf("*");
    }
    printf("\n");
}
void print_matrix(matrix board)
{
    int i,j;
    clear();
    printf(" |");
    for(i=0;i<SIZE;i++)
    {
        printf("%d|",i);
    }
    for(i=0;i<SIZE;i++)
    {
        printf("\n");
        printf("%d",i);
        for(j=0;j<SIZE;j++)
        {
            printf("|");
            if(is_alive(board,i,j))
            {
                printf("A");
            }
            else
            {
                printf("d");
            }
        }
        //clear();
    }
    clear();
}
void copy_matrix(matrix dst, matrix src)
{
    int i,j;
    for(i=0;i<SIZE;i++)
    {
        for(j=0;j<SIZE;j++)
        {
            dst[i][j] = src[i][j];
        }
    }
}

void init(matrix board)
{
    int i,j;
    srand(time(NULL));
    for(i=0;i<SIZE;i++)
    {
        for(j=0;j<SIZE;j++)
        {
            board[i][j] = rand() % 2;
        }
    }
}

int align(int  x)
{
    //Fix the indexes so it would be "cyclic"
    if(x < 0)
    {
        x = SIZE + x;
    }
    else if(x >= SIZE)
    {
        x =x % SIZE;
    }
    return x;
}

int is_alive(matrix board, int y, int x)
{
    x = align(x);
    y = align(y);

    if(x >= 0 && x < SIZE && y >= 0 && y < SIZE)
    {
        return (board[y][x] == TRUE);
    }
    printf("Wrong index! [%d][%d]\n",y,x);
    return FALSE;
}

int count_neighbors(matrix board, int y, int x)
{
    int i,j;
    int count =0;
    for(i= x-1; i<= x+1; i++)
    {
        for(j= y-1; j<= y+1; j++)
        {
            if( !(i==x && j==y))
            {
                //Not the cell itself.
                if(is_alive(board, j,i))
                {
                    count ++;
                }
            }
        }
    }
    return count;
}


int apply_rules(matrix board, int y, int x)
{
    int count = count_neighbors(board, y,x);
    int state = FALSE; //dead
    int former = is_alive(board,y,x);
    if(former == TRUE) //the cell is alive.
    {
        if(count == 2 || count == 3)
        //it would be better to use defines here, but 2 / 3 mean nothing but hard coded numbers in the rules.
        {
            state = TRUE; //the cell survives.
        }
    }
    else if(former == FALSE)//the cell is dead.
    {
        if(count == 3)
        {
            state = TRUE; //the cell is bourne again (with a shell? ;))
        }
    }
    else
    {
        printf("Unknown state! former = %d\n",former);
        printf("x,y:%d,%d\n",x,y);
        printf("#Harakiri_now! :)\n");
        print_matrix(board);
        exit(FAIL);
    }

    return state;
}
void next_stage(matrix board)
{
    int i,j;
    matrix copy;
    copy_matrix(copy, board);
    for(i=0;i<SIZE;i++)
    {
        for(j=0;j<SIZE;j++)
        {
            board[i][j] = apply_rules(copy, i,j);
        }
    }
}
