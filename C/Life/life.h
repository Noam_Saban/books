/*
Abstract:
This "library" implememts the game of life.
We have a matrix of n by n elements (**matrix is cyclic**) each has a boolean value (dead or alive).
in each stage of the game we have to iterate through the matrix and apply some rules, to determine
the state of each cell for the next stage.

The rules:
 *A dead cell with exactly three living neighbors will be ressurected.
 *A live cell with two or three living neighbors will survive.
 * In all other cases, the cell dies.

It is important to understand that we apply the changes **only after** calculating the outcome of
the current stage.
Meaning: we have two matrixes, a current one- read only, and the next one- writable.

The initial state of the matrix (before the first iteration) should not be static.
Meaning: we initialize the matrix randomally.

How:
a matrix of N*N, of type char (since we can't use single bits, and a char is the smallest, we will use that.

**IMPORTANT**:
x is j, y is i.
*/

#if !defined LIFE_H
#define LIFE_H

#include <stdio.h>
//for print_board();

#include <stdlib.h>
#include <time.h>
//for init, PRG of the board.


#define TRUE 1
#define FALSE 0

#define FAIL -1
/*
True means alive,
False means dead.
*/
#define SIZE 6

//some typedef to make our life a bit cooler.
typedef char bool;
typedef bool matrix[SIZE][SIZE];

//a psuedo random initialization of the board.
void init(matrix board);
//Print * justt to seperate stuff.
void clear();
void print_matrix(matrix board);
int count_neighbors(matrix board, int y, int x);

//Iterate over the matrix and apply the rules for each cell.
void  next_stage(matrix board);

int  align(int x);
/*
While iterating over neighboring cells, there could be problems with border line cells.
For example, the neighbors of [0,0] are:
    [-1,0][-1,-1][-1,1]
    [0,-1]       [0,1 ]
    [1,-1][-1,0 ][1,1 ]
You can clearly see that we should not access matrix[-1,0], for example.
BUT, we do want our matrix to be cyclic. so, -1 should lead to [SIZE-1].
given SIZE = 4, [-2,5,] -> [2,1] :
2 = SIZE + (-2)
1 = 5 % SIZE
*/

void copy_matrix(matrix dst, matrix src);
//Copy the src matrix to dst. a simple deep copy.

//simply return the content of a cell, since our cells hold a boolean value.
int is_alive(matrix board, int y, int x);

/*
check neighbor count and apply the following rules:
*A dead cell with exactly three live neighbors becomes a live cell (birth).
*A live cell with two or three live neighbors stays alive (survival).
*In all other cases, a cell dies or remains dead (overcrowding or loneliness).
*/
int apply_rules(matrix board, int y, int x);

#endif
