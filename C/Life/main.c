#include "life.h"

#define MAX 50
void test_init(matrix board)
{
    int i,j;
    for(i=0;i<SIZE;i++)
    {
        for(j=0;j<SIZE;j++)
        {
            board[i][j] = FALSE;
        }
    }

    board[2][2] = TRUE;
    board[3][2] = TRUE;
    board[3][1] = TRUE;
    board[4][1] = TRUE;
}
int test()
{
    //int count;
    matrix board;
    test_init(board);
    print_matrix(board);

    return 0;
}
int main()
{
    matrix board;
    int i;
    init(board);
    for(i =0;i<MAX;i++)
    {
        printf("#%d",i);
        print_matrix(board);
        next_stage(board);
        printf("\n");
    }
    return 0;
}
