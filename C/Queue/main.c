#include "queue.h"
#include <stdio.h>

//This function is just for debugging, and uses a loop (which is forbidden),
//So it is not inside the module

void printq(Queue q)
{
    Node* head = get_head(*(q.lst));
    while(head)
    {
        printf("%d ",get_item(head));
        head = get_next(head);
    }
    printf("\n");
}

int main(void)
{
    Queue q;
    int i, len;
    qinit(&q, 0);

    insert(NULL, 0);
    for(i=0;i<10;i++)
    {
        insert(&q, i+1);
        printq(q);
    }
    for(i=0,len =qlen(q);i<len;i++)
    {
        del(&q);
        printq(q);
    }
    printf("The list now:\n");
    printq(q);
    printf("\n");
    qshutdown(&q);
    return 0;
}
