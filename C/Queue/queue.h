/*
This is an extension of the list implememnation, to form a queue.
The main difference is:
    * A queue can only be added a node to the start, meaning we need to change the head.
    * A queue can only delete a node from the end, meaning we need to remove only the tail.

The queue adds another layer of abstraction to the list,
forcing its user to add/remove items in a way of FIFO.
The head -> first inserted item.
The tail -> last inserted item.
So, we remove the head of the list, and add an item as the new tail.

**Important:**
In order to compile this code you need to put list.h/.c inside this directory.
I have created symbolic links to the list files, but if it does not work, simply do it yourself.

*/


#if !defined QUEUE_H
#define QUEUE_H

#include "list.h"

typedef struct
{
    List* lst;
} Queue;

int qinit(Queue* q, item content);
/*
Allocates space for q->lst, and calls List::init().
Returns FAIL / OK.
*/

int qshutdown(Queue* q);
/*
Calls List::shutdown() and frees the space of q->lst.
Returns FAIL / OK.
*/

int insert(Queue* q, item content);
/*
add an item to the start of the queue.
returns status : OK/ FAIL.
*/

int del(Queue* q); 
/*
remove an item from the end of the queue.
upon success, returns the item. otherwise: returns FAIL.
*/

//Getters: (don't need a pointer).

int qlen(Queue q); 
//Get the length of the queue.

item peek(Queue q);
//return the first item in the queue without removing it.
#endif
