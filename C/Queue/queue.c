#include "queue.h"

int qinit(Queue* q, int content)
{
    List* tmp = NULL;
    if(q == NULL)
    {
        printf("Allocate some space for the queue struct!\n");
        return FAIL;
    }
    tmp = (List*) malloc(sizeof(List));
    if(tmp == NULL)
    {
        printf("Malloc failed\n");
        q->lst = NULL;
        return FAIL;
    }
    q->lst = tmp;
    return (init(q->lst, content));
}

int qshutdown(Queue* q)
{
    if(q != NULL)
    {
        shutdown(q->lst);
        free(q->lst);
        q = NULL;
        return OK;
    }
    return FAIL;
}

int insert(Queue* q, item content)
{
    if(q != NULL)
    {
        if(q->lst != NULL)
        {
            return (add_node(q->lst, content));
            //adds the item as the last in the queue. (our queue is reverse)
        }
    }
    return FAIL;
}

int del(Queue* q)
{
    int res = FAIL;
    if(q != NULL)
    {
        if(q->lst != NULL)
        {
            res = peek(*q);
            del_node(q->lst, q->lst->head);
            //since the queue is reversed, we keep deleting the head.
        }
    }
    return res;
}

int qlen(Queue q)
{
    if(q.lst != NULL)
    {
        return get_len(*(q.lst));
    }
    return FAIL;
}

item peek(Queue q)
{
    Node* head = NULL;
    head = get_head(*(q.lst));
    if(head != NULL)
    {
        return get_item(head);
    }
    return FAIL;
}
