#include "saurus.h"

int count_lines(FILE* fl)
{
    char ch;
    int count =0;
    if(fl == NULL)
    {
        return FAIL;
    }
    do
    {
        ch = fgetc(fl);
        if(ch == '\n')
        {
            count ++;
        }
    }while (ch!= EOF);

    fseek(fl, 0, SEEK_SET);
    //move the pos indicator to teh beginning of the file.
    //(since we just read all of it in count_lines.
    return count;
}


int parse_file(Tree* tree, FILE* fl)
{
    int i, j;
    int line_count = count_lines(fl);

    char* word = NULL;
    char* tmp = NULL;
    char* line = NULL;
    size_t read, word_len =0, status;
    size_t curr = 0;

    if(tree == NULL || line_count <= 0 || fl == NULL)
    {
        printf("~parse args are bad~\n");
        printf("line count: %d\n",line_count);
        return FAIL;
    }

    tree->arr = (List*) malloc(sizeof(List) * line_count);
    tree->size = line_count;

    for(i=0;i<line_count;i++)
    {
        j=0;
        word_len = 0;
        curr = 0;
        read = getline(&line, &read, fl);
        //printf("Line: %s",line);
        //read every line.
        do
        {
            if(curr < read)
            {
                status = sscanf( (line+curr), "%ms", &tmp);
                /*
                Notice: the m modifier is a POSIX standardized modifier.
                it should tell fscanf to malloc this string.
                */
                if(tmp != NULL)
                {
                    word_len = strlen(tmp);
                    curr += word_len +1;
                    word = (char*) malloc(sizeof(char) * word_len );
                    strcpy(word, tmp);

                    if( i < line_count)
                    {
                        if(j == 0)
                        {
                            init( &(tree->arr[i]), word);
                        }
                        else
                        {
                            add_node( &(tree->arr[i]), word);
                        }
                    }
                    //printf("\tRead :{%s} .", word);
                    //printf("The size is: %zu\n", word_len);

                    free(tmp);
                    tmp = NULL;
                }
            }
            j++;
        }while(status != EOF && curr < read);
    }
    if(tmp)
    {
        free(tmp);
        tmp = NULL;
    }
    if(line)
    {
        free(line);
        line = NULL;
    }

    return OK;
}

int destroy(Tree* tree)
{
    int i;
    List* lst = NULL;
    Node* curr = NULL;
    if( tree == NULL || tree->arr == NULL || tree->size == 0)
    {
        printf("return FAIL\n");
        return FAIL;
    }
    for(i=0;i< tree->size;i++)
    {
        lst = &(tree->arr[i]);
        curr = get_head(*lst);
        while(curr)
        {
            if(curr->content)
            {
                free(curr->content);
                curr->content = NULL;
            }
            curr = get_next(curr);
            //This is silly, since we could do curr->next, but this is a more respective approach.
        }
        shutdown(lst);
    }
    free(tree->arr);
    tree->arr = NULL;
    return OK;
}

void print(Tree tree)
{
    int i;
    List* lst = NULL;
    Node* curr = NULL;
    if(tree.arr == NULL)
    {
        return;
    }
    for(i=0;i< tree.size;i++)
    {
        lst = &(tree.arr[i]);
        curr = get_head(*lst);
        while(curr)
        {
            printf("%s ",curr->content);
            curr = get_next(curr);
        }
        printf("\n");
    }
}

int handle_file(Tree dict, const char outpath[])
{
    Tree plain;
    FILE* fl = NULL;
    int status;

    fl = fopen(outpath, READ);
    if(dict.arr == NULL || dict.size == 0 || fl == NULL)
    {
        return FAIL;
    }


    //Construct a "tree" of the plain text file, in the same method of the dictionary parsing.
    //Now we have an array of lists, each list for a different line.
    parse_file(&plain, fl);
    replace_words(dict, plain);

    fclose(fl);
    fl = fopen(outpath, WRITE); //This will "empty" the file.
    status = write_file(plain, fl);

    if(fl)
    {
        fclose(fl);
    }
    destroy(&plain); //IMPORTANT
    return status;
}
int write_file(Tree plain, FILE* fl)
{
    int i;
    List* lst = NULL;
    Node* curr = NULL;
    char* word = NULL;

    if(fl == NULL)
    {
        return FAIL;
    }

    for(i=0;i<plain.size;i++)
    {
        lst = &(plain.arr[i]);
        if(lst)
        {
            curr = get_head(*lst);
            while(curr)
            {
                word = get_item(curr);
                if(word)
                {
                    fprintf(fl, "%s ",word);
                }
                curr = get_next(curr);
            }
        }
    }
    fwrite("\n", 1,1, fl);
    return OK;
}


int replace_words(Tree dict, Tree plain)
{
    List* lst = NULL;
    Node* curr = NULL;
    int i, line;

    if(dict.size == 0 || dict.arr == NULL || plain.arr == NULL || plain.size == 0)
    {
        return FAIL;
    }

    //Iterate over **each** word of the plain struct, and search for it in the heads of the dict struct.
    for(i=0;i < plain.size; i++)
    {
        lst = &(plain.arr[i]);
        if(lst)
        {
            curr = get_head(*lst);
            while(curr)
            {
               line = search_arr(dict, get_item(curr));
               if(line != FAIL)
               {
                    //swap between the two words.
                    free(get_item(curr));
                    curr->content = random_synonym(dict, line);
               }
                if(curr)
                {
                    curr = get_next(curr);
                }
            }
        }
    }

    return OK;
}

int search_arr(Tree tree, char* word)
{
    List* lst = NULL;
    Node* head = NULL;
    int i;

    if(tree.arr == NULL || tree.size == 0 || word == NULL)
    {
        return FAIL;
    }
    for(i=0;i<tree.size;i++)
    {
        lst = &(tree.arr[i]);
        if(lst == NULL)
        {
            return FAIL;
        }
        head = get_head(*lst);
        if(head && head->content)
        {
            if(strcmp(get_item(head), word) == EQU)
            {
                return i;
            }
        }
    }
    return FAIL;
}

char* random_synonym(Tree tree, int line)
{
    int chosen, k;
    List* lst = NULL;
    Node* curr = NULL;

    if( tree.arr == NULL || tree.size == 0 ||  line > tree.size )
    {
       return NULL;
    }

    lst = &(tree.arr[line]);
    if(lst == NULL)
    {
        return NULL;
    }

    srand(time(NULL));
    chosen = rand() % (tree.size -1 ) + 1; //we never want the first word.
    curr = get_head(*lst);
    for(k=0;k< chosen && curr != NULL; k++)
    {
       curr = get_next(curr);
    }
    return (get_item(curr));
}
