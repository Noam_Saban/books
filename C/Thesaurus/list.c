#include "list.h"

int init(List* lst, char* content)
{
    if(lst == NULL)
    {
        printf("Allocate some space for the list struct!\n");
        return FAIL;
    }
    Node* tmp = NULL;
    tmp = create_node(content);
    if(tmp)
    {
        lst->head = tmp;
        lst->tail = tmp;
        lst->len =1;
        return OK;
    }
    return FAIL;
}

Node* create_node(char* content)
{
    Node* tmp = NULL;

    tmp  = (Node*) malloc(sizeof(Node));
    if(tmp)
    {
        tmp->content = content; //shallow copy!!
        tmp->next = NULL;
        tmp->prev = NULL;
    }
    return tmp;
}

int del_node(List* lst, Node* curr)
{
    Node* prev;
    Node* next;
    if(curr == NULL  || lst == NULL || lst->head == NULL)
    {
        return FAIL;
    }
    if(lst->head != curr)
    {
        prev = curr->prev;
        next = curr->next;
        prev->next = next; //Stitch the list
    }
    else
    {
        //delete the head
        next = curr->next;
        lst->head = next;
    }
    lst->len --;
    free(curr);
    curr = NULL;
    return OK;
}

int add_node(List* lst, char* content)
{
    Node* tmp = NULL;
    if(lst == NULL || lst->head == NULL || lst->tail == NULL)
    {
        printf("First call init().\n");
        return FAIL;
    }
    tmp = create_node(content);
    if(tmp)
    {
        tmp->prev = lst->tail;
        lst->tail->next = tmp;
        lst->tail = tmp; //new tail
        lst->len ++;
        return OK;
    }
    return FAIL;
}

int shutdown(List* lst)
{ //the only func we can use loops.
    Node* head =lst->head;
    Node* tmp;
    while (head)
    {
        tmp = head;
        head = head->next;
        free(tmp);
        tmp = NULL; //Don't really need this, but it is good practice.
    }
	lst->head = NULL;
    lst->tail = NULL;
    lst->len = 0;
    return OK;
}

//Getters
Node* get_head(List lst)
{
    return lst.head;
}
Node* get_tail(List lst)
{
    return lst.tail;
}
Node* get_next(Node* curr)
{
    if(curr)
    {
        return curr->next;
    }
    return NULL;
}
Node* get_prev(Node* curr)
{
    if(curr)
    {
        return curr->prev;
    }
    return NULL;
}
char* get_item(Node* curr)
{
    if(curr)
    {
        return curr->content;
    }
    return NULL; //curr->content can be == FAIL, but we can't really handle that.
}

int get_len(List lst)
{
    return lst.len;
}
