/*https://www.youtube.com/watch?v=B1tOqZUNebs

Abstract:
This prograrm acts as a thesaurus.
Given two text files: source, dictionary.
The source is just plaintext sentences.
The dictionary is seperated into lines, by synonyms.
For example
{
    fun pleasure joy;
    world planet star;
    man person guy dude;
}

How: (python implementation)
**********************
import random

dict = { "fun": ["pleasure", "joy"], "world": ["planet","star"], "man": ["person", "guy", "dude"] }
plain = "The nice man says he has the most fun on this world."
words = plain.split(' ')

for word in words:
    if word in dict:
        random.shuffle(dict[word])
        copy += [dict[word][0]]
    else:
        copy += [word]

**********************
*/

#if !defined SAURUS_H
#define SAURUS_H

#define EQU 0
#define READ "r"
#define WRITE "w"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "list.h"

typedef struct Tree_struct
{
    List* arr; //an array of list elements.
    int size;
}Tree;

int count_lines(FILE* fl);
//Return the number of lines in the file fl.

int parse_file(Tree* tree, FILE* fl);
/*
Creates a linked list of each line from the file.
Each node in each list is a different word (the lines are seperated by spaces).
Allocates the needed array of lists, and the strings to be inserted as nodes.
Writes the size of the allocated array into *size, for a later cleanup.
*/

int destroy(Tree* tree);
//frees everything that parse_file allocted.

void print(Tree tree);
//Iterates over a tree (all of its lists) and prints each node (char*).

int handle_file(Tree dict, const char outpath[]);
/*
The ~main~ function for rewriting the text file (with the synonyms).
Parses the text file using parse_file, so we have an array of lists,
Each list for another line.
We don't really need the array here, could use a long list of words,
but.. we have a func for that already.
*/

int write_file(Tree plain, FILE* fl);
/*
Put the fl pos indicator to the start,
and write all of the words on plain into the file.
*/
int replace_words(Tree dict, Tree plain);
/*
Iterates over the dictionary tree, searching for matches in the text (using search_arr).
If such matches are found, calls random_synonym, which returns a sysonym to be use instead of the original word.
*/
int search_arr(Tree tree, char* word);

char* random_synonym(Tree tree, int i);

#endif
