#include "saurus.h"

#define SIZE 100

int main(int argc, const char* argv[])
{
    FILE* dict_file = NULL;
/*
    const char path[] = "dict"; //relative path.
    const char outpath[] = "text";
*/
    Tree dict;

    if(argc != 3)
    {
        printf("No input/ output files specified. Aborting.\n");
        printf("Usage : %s dictonary_file text_file\n",argv[0]); //argv[0] must always exist..
        return -1;
    }
    dict_file = fopen(argv[1], READ);
    if(dict_file == NULL)
    {
        perror("Cannot open the file!\n");
        return -1;
    }

    parse_file(&dict, dict_file);
    handle_file(dict, argv[2]); //argv[2] is the text file.

    destroy(&dict);
    if(dict_file)
    {
        fclose(dict_file);
    }
    return 0;
}
