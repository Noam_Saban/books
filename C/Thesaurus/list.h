/*
 This library implements a linked list data structure.
 The list holds char*s of type "char*", which is a typedef for char*.
 */

/*
IMPORTANT:
This is the library from question #1, but in an adapted version.
The main difference is that this version holds strings, and not ints.
This is, so we can

*/

#if !defined(LIST_H)
#define LIST_H

#include <malloc.h>

#define OK 1
#define FAIL -1


//A typedef for easy replacement of the linked list object type.c

//A struct to represent a single node in the list.
typedef struct list_node
{
    char* content; //The content of char* on the node.
    struct list_node* prev; //Previous node on the list.
    struct list_node* next; //Next node on the list.
} Node;

//A struct to represent a complete list.
typedef struct entire_list
{
    Node* head; //first node of the list.
    Node* tail; //last node of the list.
    int len;    //number of nodes.
} List;

/*
This function allocates the head of the list, and stores its pointer in the static variable.
Upon success, the function shall return a value defined as INIT_OK
Upon failure, the function shall return INIT_FAIL.
*/
int init(List* lst, char* content);

//A wrapper for allocating a node and initializing its fields.
Node* create_node(char* content);

/*
This function frees the entire linked list.
*/
int shutdown(List* lst);

//add a single node to the end of the list.
int add_node(List* lst, char* content);

//remove a node by a given pointer.
int del_node(List* lst, Node* curr);

//getters:

//Get the head of the list.
Node* get_head(List lst);
//Get the tail of the list.
Node* get_tail(List lst);
//Given a pointer to a node, get the next node.
Node* get_next(Node* curr);
//Given a pointer to a node, get the previous node.
Node* get_prev(Node* curr);
//Given a pointer to a node, get the contained char*.
char* get_item(Node* curr);

//Since we can't iterate over the entire list to sum the nodes,
//we save the sum of the list nodes.
//This function simply calculates the average of n elements with a given sum.
int get_len(List lst);

#endif
