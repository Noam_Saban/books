/*
 This library implements a linked list data structure.
 The list holds items of type "item", which is a typedef for int.
 As asked, all of the functions work without loops, besides the deconstruction of the entire list.
 */

#if !defined(LIST_H)
#define LIST_H

#include <malloc.h>

#define OK 1
#define FAIL -1


//A typedef for easy replacement of the linked list object type.c
typedef int item;

//A struct to represent a single node in the list.
typedef struct list_node
{
    item content; //The content of item on the node.
    struct list_node* prev; //Previous node on the list. 
    struct list_node* next; //Next node on the list.
} Node;

//A struct to represent a complete list.
typedef struct entire_list
{
    Node* head; //first node of the list.
    Node* tail; //last node of the list.
    int len;    //number of nodes.
    item sum;   //In order to calculate the average, we need to keep the sum.
                //At each insertion / deletion we change the sum. Then the avg func just uses that.
} List;

/*
This function allocates the head of the list, and stores its pointer in the static variable.
Upon success, the function shall return a value defined as INIT_OK
Upon failure, the function shall return INIT_FAIL.
*/
int init(List* lst, item content);

//A wrapper for allocating a node and initializing its fields.
Node* create_node(item content);

/*
This function frees the entire linked list.
*/
int shutdown(List* lst);

//add a single node to the end of the list.
int add_node(List* lst, item content);

//remove a node by a given pointer.
int del_node(List* lst, Node* curr);

//getters:

//Get the head of the list.
Node* get_head(List lst);
//Get the tail of the list.
Node* get_tail(List lst);
//Given a pointer to a node, get the next node.
Node* get_next(Node* curr);
//Given a pointer to a node, get the previous node.
Node* get_prev(Node* curr);
//Given a pointer to a node, get the contained item.
item get_item(Node* curr);

//Since we can't iterate over the entire list to sum the nodes,
//we save the sum of the list nodes.
//This function simply calculates the average of n elements with a given sum.
float get_avg(List lst);
int get_len(List lst);

#endif
