#include "list.h"
#include <stdio.h>

//This function is just for debugging, and uses a loop (which is forbidden),
//So it is not inside the module
void print_list(List lst)
{
    Node* head = get_head(lst);
    printf("the list:\n");
    while(head)
    {
        printf("%d ",get_item(head));
        head = get_next(head);
    }
    printf("\n*********************\n");
}

void reverse(List lst)
{
    Node* tail = get_tail(lst);
    printf(":tsil eht\n");
    while(tail)
    {
        printf("%d ",get_item(tail));
        tail = get_prev(tail);
    }
    printf("\n*********************\n");

}

void print(int type)
{
    switch (type)
    {
        case OK:
        {
            printf("OK!\n");
            break;
        }
        case FAIL:
        {
            printf("Error!\n");
            break;
        }
    }
}

void print_avg(List lst)
{
    printf("The average of the list is : %f\n",get_avg(lst));
}

int main(void)
{
    /*
    int i;
    List one, two;
    printf("Trying to add before init..\n");
    print(add_node(NULL, 42));
    
    printf("Checking normal init\n");
    print(init(&one, 10));
    print_list(one);
    print_avg(one);

    printf("Constructing the list:\n");
    add_node(NULL,0);
    for(i=0;i<10;i++)
    {
        add_node(&one, 10);
        print_list(one);
        print_avg(one);
    }

    print_list(one);
    print_avg(one);
    reverse(one);

    shutdown(&one);
    printf("Checking double deletion:\n");
    print(shutdown(&one));
    */
    List lst;
    int i;
    init(&lst, -1);
    for(i=0;i<10;i++)
    {
        add_node(&lst, i);
    }
    print_list(lst);
    del_node(&lst, get_head(lst));

    print_list(lst);

    shutdown(&lst);
    return 0;
}
