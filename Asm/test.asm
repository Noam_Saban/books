global _start

section .data
str:
    db "Hello world",0

section .text

_start:

    ;ssize_t write(int fd, const void *buf, size_t count);
    mov eax,4 ;write syscall
    mov ebx,1 ; write to stdout
    mov ecx, str
    mov edx,11 ; length of the string
    int 0x80 ; int no. 0x80 is syscall.

    ;exit:
    mov eax,1 ;exit syscall
    mov ebx,0 ;ret val
    int 0x80
