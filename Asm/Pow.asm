global _start

section .text

_start:
    push ebp
    mov ebp, esp

    push dword 5; base
    push dword 7; exponent
    call pow

    mov esp,ebp
    pop ebp

    ;exit:
    mov eax,1 ;exit syscall
    mov ebx,0 ;ret val
    int 0x80

param1 equ 12
param2 equ 8
var equ -4

pow:
    push ebp
    mov ebp,esp
    sub esp,4 ;make room for our local var.
    pusha

    mov eax, [ebp + param1]
    mov ebx, eax
    mov ecx, [ebp + param2]
    dec ecx

_loop:
    mul ebx
    loop _loop
    mov [ebp + var], eax
    popa
    mov eax, [ebp + var]

    mov esp,ebp
    pop ebp
    ret
