global _start

section .data
str:
    db "Error" ,10, 0

section .text

_start:
    push ebp
    mov ebp,esp

    push -1
    call fibo

    mov esp,ebp ;clean the stack
    pop ebp

    ;exit:
    mov eax,1 ;exit syscall
    mov ebx,0 ;ret val
    int 0x80

param1 equ 8
res equ -4

fibo:
    push ebp
    mov ebp,esp
    sub esp, 4 ;a dword local variable
    pushad

    mov eax, [ebp+ param1]
    cmp eax,0
    je ONE
    jl ERROR
    cmp eax,1
    je ONE
    jmp NEXT_CALL

    ONE:
        mov [ebp+res], dword 1 ;push the result
        jmp END

    NEXT_CALL:
        ;call fibo(n-1)
        dec eax
        push eax
        call fibo
        pop ecx ; remove the value we passed to the func.
        mov [ebp+ res], eax ; save fibo(n-1)

        ;call fibo(n-2)
        mov eax, [ebp+ param1]
        dec eax
        dec eax
        push eax
        call fibo
        pop ecx ; remove the value we passed to the func.
        ;nasm does not allow an empty pop, so throw it to ecx.
        add [ebp+ res], eax ; res = fibo(n-1) + fibo(n-2)

    END:
        popad
        mov eax, [ebp+res] ;put the result in eax

    mov esp,ebp ;clean the stack
    pop ebp
    ret

ERROR:
    ; to prevent a stack overflow
    ; if the parameter is negative, simply exit.
    mov eax,4 ;write syscall
    mov ebx,1 ; write to stdout
    mov ecx, str
    mov edx,6 ; length of the string
    int 0x80 ; int no. 0x80 is syscall.

    mov eax,1 ;exit syscall
    mov ebx,0 ;ret val
    int 0x80

