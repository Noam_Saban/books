global _start

section .data
    str_a: db "Goodbye world",0,0
    str_b: db "Hello world",0,0
    str_ok: db "Strings match", 10, 0
    str_no: db "Strings do not match", 10, 0

section .text

_start:
    push ebp
    mov ebp,esp

    lea eax, [str_ok]
    push eax
    lea eax, [str_ok]
    push eax
    call strcmp

    cmp eax, 0xffff
    je OK
NO:
    mov eax,4 ;write syscall
    mov ebx,1 ; write to stdout
    mov ecx, str_no
    mov edx,21 ; length of the string
    int 0x80 ; int no. 0x80 is syscall.
    jmp EXIT
OK:
    mov eax,4 ;write syscall
    mov ebx,1 ; write to stdout
    mov ecx, str_ok
    mov edx,14 ; length of the string
    int 0x80 ; int no. 0x80 is syscall.

EXIT:
    mov esp, ebp
    pop ebp
    ;exit:
    mov eax,1 ;exit syscall
    mov ebx,0 ;ret val
    int 0x80

;;;;;;;;;;;;;;;;;;;;;
param1 equ 12
param2 equ 8
var equ -4

strcmp:
    push ebp
    mov ebp,esp
    sub esp, 4
    pusha

    mov esi, [ebp+param1]
    mov edi, [ebp+param2]
    mov [ebp + var], dword 0xffff

    dec esi
    dec edi

LOOP:
    inc esi
    inc edi
    mov bl, [esi]
    mov bh, [edi]
    cmp bl,bh
    ;cmpsb ;compares a byte of [ESI] with [EDI]
    jne NO_MATCH
    cmp bl, byte 0 ; check if [esi] points to null
    je END
    jmp LOOP

NO_MATCH:
    mov [ebp+var], dword 0

END:
    popa
    mov eax, [ebp + var]
    mov esp,ebp
    pop ebp
    ret
