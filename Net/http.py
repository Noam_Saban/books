#!/usr/bin/python2.7

from socket import *
SIZE = 1024

def parse(sock):
    while True:
        path = "/home/noam/Desktop"
        (c_sock, addr) = sock.accept()
        print "connected to: " , addr
        cmd = c_sock.recv(SIZE)
        new_parts =[]
        parts = cmd.split(' ')
        new_parts = [i.replace('\n','') for i in parts]
        parts = new_parts

        if len(parts) >= 2 and parts[0] == "GET" and ".." not in parts[1]:
            #path traversal? not on my server.
            print parts[1]
            data=""
            try:
                req = path + parts[1]
                print "requested: ",req
                fl = open(req, 'r')
                content = fl.read()
                data = 'HTTP/1.0 200 Document Follows\n' + \
                'Content-length:'+ str(len(content)) +'\n' +'\n' + content
                fl.close()
            except IOError:
                print "Caught!"
                data = "HTTP/1.0 404 Page not found\n\n"
            c_sock.sendall(data)

        c_sock.shutdown(SHUT_RDWR)
        c_sock.close()

def main():
    sock = socket(AF_INET, SOCK_STREAM)
    sock.bind(("0.0.0.0",80)) #http
    sock.listen(1)
    try:
       parse(sock)
    except KeyboardInterrupt:
        print "shutting down"

    sock.shutdown(SHUT_RDWR)
    sock.close()


if __name__=="__main__":
    main()
