#!/usr/bin/python2.7
from socket import *

ADDR = "mail.barak.net.il"
PORT = 25
SIZE = 1024


def main():
    sock = socket(AF_INET, SOCK_STREAM)
    sock.connect((ADDR,PORT))
    if login(sock) != -1:
        send_mail(sock)

    sock.close()

def login(sock):
    data = sock.recv(SIZE)
    if data[:3] != "220":
        print "Could not connect"
        print data
        return -1

    sock.send("HELO " + ADDR+"\r\n")
    print "After hello"
    data = sock.recv(SIZE)
    print data
    if data[:3] != "250":
        print "Error"
        print data
        return -1
    return 1

def send_mail(sock):
    from_addr = raw_input("Enter sender address: ")
    to = raw_input("Enter receiver address: ")
    if from_addr == "":
        print "Error"
        return -1
    sock.send("MAIL From: " +from_addr +"\n")
    sock.send("RCPT To: "+ to +"\n")
    sock.send("DATA"+"\n")
    sock.send("From: "+from_addr+"\n")
    sock.send("To: "+to+"\n")
    sub = raw_input("Enter the subject: ")
    sock.send("Subject: " + sub+"\n")
    content = raw_input("Type your message: ")
    while content != "":
       sock.send(content +"\n")
       content = raw_input()
    sock.send(".\n")
    data = sock.recv(SIZE)
    if data[:3] != "250":
        print "The server did not accept"
        print data


if __name__=="__main__":
    main()

