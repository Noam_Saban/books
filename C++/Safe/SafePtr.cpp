template<class T>
SafePtr<T>::SafePtr(int size, int idx, T* ptr): ptr(ptr), idx(idx), size(size)
{
    if(idx >= size || idx < 0 )
    {
        throw RangeEx(idx);
    }
}

template<class T>
T& SafePtr<T>::operator[](int index)const
{
    if(index >= size || index < 0 )
    {
        throw RangeEx(index);
    }
    if(ptr == NULL)
    {
        throw InvalidEx();
    }

    return ptr[index];
}

template<class T>
SafePtr<T>& SafePtr<T>::operator=(const SafePtr<T>& other)
{
    this->size = other.GetSize();
    this->idx = other.GetIndex();
    this->ptr = const_cast<T*>(other.GetPtr()); //Hey, it works!
    return *this;
}

template<class T>
SafePtr<T>& SafePtr<T>::operator=(const T& val)
{
    if(idx >= size || idx < 0 || ptr == NULL)
    {
        throw RangeEx(idx);
    }
    ptr[idx] = val;
    return *this;
}

template<class T>
SafePtr<T>& SafePtr<T>::operator++(int)
{
    if( (idx) >= size)
    {
        throw RangeEx(idx+1);
    }
    idx++;
    return *this;
}

template<class T>
SafePtr<T>& SafePtr<T>::operator--()
{
    if( (idx-1) < 0)
    {
        throw RangeEx(idx+1);
    }
    idx--;
    return *this;
}

template<class T>
bool SafePtr<T>::operator==(const SafePtr<T>& other)const
{
    return ( (this->ptr == other.GetPtr()) && (this->idx == other.GetIndex() ) );
}

template<class T>
bool SafePtr<T>::operator!=(const SafePtr<T>& other)const
{
    if(this->ptr == other.GetPtr())
    {
        return (this->index == other.GetIndex());
    }
    return false;
}


template<class T>
bool SafePtr<T>::operator<(const SafePtr<T>& other)const
{
    if(this->ptr == other.GetPtr())
    {
        return (this->idx < other.GetIndex());
    }
    return false;
}

template<class T>
bool SafePtr<T>::operator>(const SafePtr<T>& other)const
{
    if(this->ptr == other.GetPtr())
    {
        return (this->index > other.GetIndex());
    }
    return false;
}

template<class T>
bool SafePtr<T>::operator<=(const SafePtr<T>& other)const
{
    return (*this < other || *this == other);
}

template<class T>
int SafePtr<T>::GetIndex()const
{
    return idx;
}

template<class T>
int SafePtr<T>:: GetSize()const
{
    return (this->size);
}

template<class T>
const T* SafePtr<T>::GetPtr()const
{
    return this->ptr;
}
