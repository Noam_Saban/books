template<class T>
SafeArr<T>:: SafeArr(int size): SafePtr<T>(size,0,NULL)
{
    if(size <= 0)
    {
        throw SizeEx(size);
    }
    this->ptr = new T[size]; //since we had to put NULL in the ctor.
}

template<class T>
SafeArr<T>::SafeArr(const SafeArr& other): SafePtr<T>(other.GetSize(), 0,NULL)
{
    int i;
    this->ptr = new T[this->size];
    for(i=0;i<this->size;i++)
    {
        this->ptr[i] = other[i];
    }
}
template<class T>
SafeArr<T>:: ~SafeArr()
{
    delete[] (this->ptr);
    this->size =0;
    this->idx = 0;
}


template<class T>
void SafeArr<T>::print()const
{
    int i;
    for(i=0;i<this->size;i++)
    {
        std::cout <<this->ptr[i]<<std::endl;
    }
}

template<class T>
SafePtr<T> SafeArr<T>::begin()const
{
    SafePtr<T> tmp(this->size,0, this->ptr);
    return tmp;
}

template<class T>
SafePtr<T> SafeArr<T>::end()const
{
    SafePtr<T> tmp(this->size, this->size -1, this->ptr);
    return tmp;
}
