#include <iostream>

#include "SafeArr.h"
#define SIZE 10
int main(void)
{
    int i, foo;
    try
    {
        SafeArr<std::string> strings(SIZE);
        SafeArr<int> numbers(SIZE);
        for(i=0;i<numbers.GetSize(); i++)
        {
            strings[i] = ("number: " + std::to_string(i));
            numbers[i] = i;
        }

        std::cout<<"Numbers:"<<std::endl;
        numbers.print();
        strings.print();
        //Example of exception throwing usage.
        //numbers[999] = 7;

        SafePtr<int> ptr = numbers.begin();
        std::cout <<"By SafePtr:" <<std::endl;
        for(; ptr <= numbers.end(); ptr++)
        {
            std::cout<<"Index: " <<ptr.GetIndex() <<std::endl;
            ptr = -1;
        }

        std::cout<<"Final:"<<std::endl;
        numbers.print();


        numbers[0] = 42;
        foo = numbers[0];
        std::cout<<"foo:"<<foo<<std::endl;
        foo = 3; //this will cause numbers[0] to change too.
        std::cout<<numbers[0] <<std::endl;

        ptr = numbers.end();
        while(true)
        {
            ptr = 3;
            ptr++;
        }
        //Exception throwing operations:
        /*
        numbers[-1] = 42;
        numbers[200] = 13;
        */

    }
    catch(Ex& a)
    {
        std::cout <<a.what() <<std::endl;
    }
    return 0;
}
