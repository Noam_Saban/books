#if !defined SAFE_PTR_H
#define SAFE_PTR_H

#include "Exceptions.h"

template<class T>
class SafePtr
{
public:
    SafePtr(int size, int idx, T* ptr);
    //~SafePtr(); //don't need to clean anything.
    T& operator[](int index)const; //This operator does not change the member idx.
    SafePtr<T>& operator=(const SafePtr<T>& other);
    SafePtr<T>& operator=(const T& val);
    SafePtr<T>& operator++(int);
    SafePtr<T>& operator--();
    bool operator==(const SafePtr<T>& other)const;
    bool operator!=(const SafePtr<T>& other)const;
    bool operator<(const SafePtr<T>& other)const;
    bool operator>(const SafePtr<T>& other)const;
    bool operator<=(const SafePtr<T>& other)const;

    int GetIndex()const;
    int GetSize()const;
    const T* GetPtr()const;

protected:
    T* ptr; //actual pointer.
    int idx; //The index the pointer is currently pointing too.
    int size; //the size of the array
};

#include "SafePtr.cpp"
#endif
