#if !defined EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <string>

/*
This file will declare and implement a small number of exceptions for "daily use" in our program.

*/

class Ex: public std::exception
{
public:
    Ex(std::string err): err(err)
    {}
    virtual const char* what() const throw()
    {
        const char* foo = err.data();
        return foo;
    }
protected:
    std::string err;
};

class SizeEx: public Ex
{
public:
    SizeEx(int size): Ex("foo"), size(size)
    {
        err = ("Cannot initialize array with size " + std::to_string(size));
    }
protected:
    int size;
};

class RangeEx: public Ex
{
public:
    RangeEx(int idx): Ex("foo"), idx(idx)
    {
        err = ("Cannot access array element " + std::to_string(idx));
    }
protected:
    int idx;
};

class InvalidEx: public Ex
{
public:
    InvalidEx(): Ex("Cannot access pointer of NULL!")
    {}
};
#endif
