#if !defined SAFE_ARR_H
#define SAFE_ARR_H

#include "SafePtr.h"

/*
This is question #2 at the C++ chapter, the safe array.
The idea is to implement an array wrapper (non dynamic in size)
so that instead of getting segfault from the OS, the array will throw an exception
when accessing out of range, for example.

The array hold N elements all of the same type (templates anyone?)
 */

template<class T>
class SafeArr : public SafePtr<T>
{
public:
    SafeArr(int size); //Ctor
    SafeArr(const SafeArr& other); //CopyCtor
    ~SafeArr(); //Dtor

    //operatos:
    SafeArr& operator=(const SafeArr& other)const;

    //getters:
    void print()const;
    SafePtr<T> begin() const; //returns a safe iterator to the first element.
    SafePtr<T> end() const; //returns a safe iterator to the last element.
};


/*
The book suggested using a single (.h) file for both
the class decleration and its implementation.
That isn't too nice, and by a bit of googling I found a different solution.
http://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file#495056

I still have h and cpp, but my h defines the class,
then includes the cpp for the implementation.

The include must be after the decleration and not at the head of the file,
otherwise the cpp still does not know what is type (SafeArr).
*/
#include "SafeArr.cpp"

#endif
