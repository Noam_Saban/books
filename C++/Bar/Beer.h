#if !defined BEER_H
#define BEER_H

#include "Drink.h"

class Beer: public Drink
{
public:
    Beer(std::string name); //Ctor
    virtual void prepare() const; //The beer implementation of Drink::perpare().
};


#endif
