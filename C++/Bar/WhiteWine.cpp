#include "WhiteWine.h"

WhiteWine:: WhiteWine(std::string name, int year): Wine(name, year)
{}

void WhiteWine:: prepare() const
{
    std::cout << "You ordered " <<name <<std::endl;
    std::cout << "White wines are served cooled." <<std::endl;

}
