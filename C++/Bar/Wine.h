#if !defined WINE_H
#define WINE_H

#include <exception>
#include "Drink.h"

//An abstract class, derived from Drink.
class Wine : public Drink
{
public:
    Wine(std::string name, int year); //Default year for wines.
    virtual std::string GetName() const;
    //Im not sure about the conventions here. (should I redeclare this func, or not?)
    //Need to ask.
    virtual void prepare() const =0;

protected:
    int year;
};

//Not sure if I really need this.
/*
class InvalidYear : public std::exception
{
public:
    InvalidYear(int year): year(year)
    {
    }
    const char* what()
    {
        std::string err = "The year" + std::string(year) + " is not a valid year.";
        return err.cstr();
    }

protected:
    int year;
};
*/

#endif
