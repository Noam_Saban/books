#if !defined BAR_H
#define BAR_H

/*
This is the third question of the C++ chapter questions.
There is a "small" bug in Bar::Serve() , due to std::cin.
The bug is that the program expects some numeric input, (in a loop)
and having a char inserted just makes std::cin go berserk, keeping the program in that loop.
Well, it should be very possible to fix this, but I don't have the time right now.
So, gonna hope to catch up on that some time later.
 
 */
#include "WhiteWine.h"
#include "RedWine.h"
#include "Beer.h"

#define EXIT 100
#define EXP 99
class Bar
{
public:
    Bar(); //Ctor, allocate the drink array.
    ~Bar(); //Dtor, release the array.
    void serve(); //The main func for the client entering the bar.

private:

    //Members:
    Drink** stock;
    const int size;
    int last;

    //Methods:
    void PrintStock()const;
};

#endif
