#if !defined RED_WINE_H
#define RED_WINE_H
#include "Wine.h"

class RedWine: public Wine
{
public:
    RedWine(std::string name, int year);
    virtual void prepare() const; //non pure virtual
};

#endif
