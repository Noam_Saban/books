#include "Beer.h"

Beer ::Beer(std::string name) : Drink(name)
{
    //Nothing :)
}
void Beer::prepare() const
{
    std::cout <<"You ordered " <<name <<std::endl;
    std::cout <<"Well, all you need to do is pour it into a glass and serve." <<std::endl;
}
