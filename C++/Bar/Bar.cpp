#include "Bar.h"

Bar:: Bar() : size(11)
{
    //Special year wines.
    stock = new Drink*[size];
    //if new failes, we can't do much.

    stock[0] = new WhiteWine("Carmel emerald riesling", 1986);
    stock[1] = new WhiteWine("Golan smadar", 1989);
    stock[2] = new RedWine("Har hermon", 1994);

    //The rest of the wines.
    stock[3] = new WhiteWine("Yarden chardonnay", 1997);
    stock[4] = new RedWine("Chianti wine",1997);
    stock[5] = new RedWine("Cabernet sauvignon", 1997);
    stock[6] = new RedWine("Chadeau margot", 1997);

    //Beers:
    stock[7] = new Beer("Heineken");
    stock[8] = new Beer("Goldstar");
    stock[9] = new Beer("Maccabi");
    stock[10] = new Beer("Tuborg");
}

Bar:: ~Bar()
{
    int i ;
    if(stock)
    {
        for(i=0;i < size; i++)
        {
            delete stock[i];
            stock[i] = NULL;
        }

        delete[] stock;
        stock = NULL;
    }
}
void Bar::PrintStock()const
{
    int i;
    std::cout <<"(0)\tList options" <<std::endl;
    for(i=0; i< size; i++)
    {
        if(stock[i])
        {
            std::cout << "(" << i+1 << ")\t" << stock[i]->GetName() <<std::endl;
        }
    }

    std::cout <<"(99)\tHow did you prepare my last drink?" <<std::endl;
    std::cout <<"(100)\tLeave the bar" <<std::endl <<std::endl<<std::endl;
}

void Bar:: serve()
{
    int choice=0;
    int last = -1;

    do
    {
        std::cout << "(0)\t List options" <<std::endl;
        std::cin >> choice;
    }while(choice != 0);

    do
    {
        PrintStock();
        std::cin >> choice;
        if(choice == EXP)
        {
            if(last != -1)
            {
                    stock[last]->prepare();
            }
            else
            {
                std::cout <<"You never had a drink!"<<std::endl;
            }
        }
        else if(choice != EXIT)
        {
            last = choice -1;
        }
    }while(choice != EXIT);
}
