#include "Bar.h"

int old_main(void)
{
    Beer b("Stella");
    RedWine rw("Golan", 1989);
    WhiteWine ww("IDK", 1999);
    std::cout <<b.GetName() <<std::endl;
    std::cout <<rw.GetName() <<std::endl;
    std::cout <<ww.GetName() <<std::endl;
    return 0;
}

int main(void)
{
    Bar bar;
    bar.serve();

    return 0;
}
