#if !defined WHITE_WINE_H
#define WHITE_WINE_H

#include "Wine.h"

class WhiteWine: public Wine
{
public:
    WhiteWine(std::string name, int year);
    virtual void prepare () const; //non pure virtual.
};

#endif
