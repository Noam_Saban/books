#if !defined DRINK_H
#define DRINK_H

#include <string>
#include <iostream>

/*
An abstract class, representing a drink.
Will be the base class for the mny drinks we will have on this question :)
*/
class Drink
{
public:
    Drink(std::string name); //Ctor
    virtual ~Drink();
    /*
    a virtual Dtor, becuase we need to delete objects of non-Drink by Drink* .
    For more info:
    http://stackoverflow.com/questions/461203/when-to-use-virtual-destructors
    */ 
    virtual std::string GetName() const; //A virtual function to be reimplemented.
    virtual void prepare() const =0; //How do you prepare an abstract drink?

protected:
    std::string name; //The only class member- holding the drink name.
};


#endif
