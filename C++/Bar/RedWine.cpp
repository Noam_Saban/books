#include "RedWine.h"

RedWine:: RedWine(std::string name, int year): Wine(name, year)
{}

void RedWine:: prepare() const
{
    std::cout << "You ordered " <<name <<std::endl;
    std::cout << "Red wines are served at room temperature (16-18 degrees)" <<std::endl;
}
