#if !defined DIV_H
#define DIV_H

#include "Oper.h"

class Div: public Operation
{
public:
    Div(); //A ctor that calls the base ctor.
    virtual double calc(double curr, std::vector<std::string> parameters);

private:
    std::vector<std::string> build_vec();
};
#endif
