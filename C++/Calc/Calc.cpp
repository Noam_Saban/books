#include "Calc.h"

Calc:: Calc() : reg(0)
{
    try
    {
        ops.push_back(new Add);
        ops.push_back(new Set);
        ops.push_back(new Sub);
        ops.push_back(new Mul);
        ops.push_back(new Div);
        ops.push_back(new Pow);
        ops.push_back(new Sqr);
    }
    catch(std::exception& a)
    {
        std::cout << "Error!" <<std::endl;
    }
}

Calc::~Calc()
{
    std::vector<Operation*>::iterator it;
    for(it=ops.begin(); it != ops.end();)
    {
        if(*it)
        {
            delete *it;
            ops.erase(it);
        }
        else
        {
            it++;
        }
    }
}

std::vector<Operation*>::iterator Calc::find(std::vector<std::string> params)
{
    std::vector<Operation*>::iterator it;
    for(it = ops.begin(); it != ops.end(); it++)
    {
        if( *it && (*it)->support(params))
        {
            return it;
        }
    }
    return it; //should point to end() now, so that's a good sign of failure.
}
void Calc:: run()
{
    //This is a simple example of how things are gonna work.
    std::string line;
    std::vector<Operation*> ::iterator it;
    std::vector<std::string> parameters;

    do
    {
        getline(std::cin, line);
        if(line != "quit" && !line.empty())
        {
            parameters = Operation::split(line);
            //Operation::print(parameters);
            it = find(parameters);
            if(it != ops.end() && *it)
            {
                reg =(*it)->calc(reg, parameters);
            }
            else
            {
                std::cout << "Operation not found!"<<std::endl;
            }
            std::cout <<"Current value is:" <<reg <<std::endl;
        }
    }while(line != "quit");
}
