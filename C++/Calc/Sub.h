#if !defined SUB_H
#define SUB_H

#include "Oper.h"

class Sub: public Operation
{
public:
    Sub(); //A ctor that calls the base ctor.
    virtual double calc(double curr, std::vector<std::string> parameters);

private:
    std::vector<std::string> build_vec();
};
#endif
