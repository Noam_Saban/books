#if !defined OPER_H
#define OPER_H

#include <iostream>
#include <string>
#include <vector>

/*
This is an abstract base-class for our modular calculator.
All operations will inherit from this class, and implement its methods.

I took some liberty in this question.
in your instructions, the func support() receives a single string,
but since I am processing the user input in a different way (read the comment at Calc.h)
it suits much better to pass support a vector of strings.
*/
class Operation
{
public:
    //Ctor:
    Operation(std::vector<std::string> operators, int required);
    virtual ~Operation();

    //methods:
    bool support(std::vector<std::string> parts);
    int GetRequired()const ;
    virtual double calc(double curr, std::vector<std::string> parameters)=0;

    //methods:
    static std::vector<std::string> split(std::string line);
    //a static method to wrap splitting a string by spaces.
    static void print(std::vector<std::string> parameters);
protected:
    //members:
    std::vector<std::string> operators;
    unsigned int required;
    //It should be clear that the number of operators is WITH the
    //operation operator itself.
};
#endif
