#include "Oper.h"

Operation::Operation(std::vector<std::string> operators, int required)
{
    if(operators.empty() || required == 0)
    {
        //throw Invalid();
    }
    this->operators = operators;
    this->required = required;
}

Operation ::~Operation()
{}

bool Operation::support(std::vector<std::string> parts)
{
    bool found = false;
    std::vector<std::string>::iterator it;
    if(parts.size() != required || parts.empty())
    {
        return false;
    }
    for(it= operators.begin(); it != operators.end(); it++)
    {
        if(parts.front() == *it)
        {
            found = true;
        }
    }
    return (found && parts.size() == required);
}

std::vector<std::string> Operation::split(std::string line)
{
    std::vector<std::string> parts;
    const std::string delim=" ";
    size_t pos = 0;
    std::string token;

    //std::cout << line <<std::endl;

    while( (pos = line.find(delim)) != std::string::npos)
    {
        token = line.substr(0,pos);
        if(!token.empty())
        {
            parts.push_back(token);
        }
        line.erase(0,pos+1);
    }
    if(!line.empty()) //since there is no terminating space, we need to the rest of the line.
    {
        parts.push_back(line);
    }
    return parts;
}

void Operation::print(std::vector<std::string> parameters)
{
    std::vector<std::string>::iterator it;
    for(it = parameters.begin(); it!= parameters.end(); it++)
    {
        std::cout <<*it <<std::endl;
    }
}
