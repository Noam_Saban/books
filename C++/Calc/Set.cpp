#include "Set.h"

Set::Set() : Operation(build_vec(), 2)
{}

double Set::calc(double curr, std::vector<std::string> parameters)
{
    double val =0;
    curr ++;
    //Compiling with Weror makes g++ hate me for not using a variable/ parameter.
    //So, this is some band aid so that g++ will accept my unused parameter curr.
    if(parameters.size() != required)
    {
        std::cout << "Not supported!" <<std::endl;
    }
    try
    {
       val = std::stod(parameters.at(1)); //the second parameter.
    }
    catch(std::exception& a)
    {
        std::cout << a.what() <<std::endl;
    }
    return val;
}
std::vector<std::string> Set::build_vec()
{
    std::vector<std::string> tmp;
    tmp.push_back("set");
    tmp.push_back("=");
    return tmp;
}
