#if !defined POW_H
#define POW_H

#include <cmath>
#include "Oper.h"

class Pow: public Operation
{
public:
    Pow(); //A ctor that calls the base ctor.
    virtual double calc(double curr, std::vector<std::string> parameters);

private:
    std::vector<std::string> build_vec();
};
#endif
