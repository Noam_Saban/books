#include "Mul.h"

Mul::Mul() : Operation(build_vec(), 2)
{}

double Mul::calc(double curr, std::vector<std::string> parameters)
{
    double val =0;
    if(parameters.size() != required)
    {
        std::cout << "Not supported!" <<std::endl;
    }
    try
    {
       val = std::stod(parameters.at(1)); //the second parameter.
    }
    catch(std::exception& a)
    {
        std::cout << a.what() <<std::endl;
    }
    return curr*val;
}
std::vector<std::string> Mul::build_vec()
{
    std::vector<std::string> tmp;
    tmp.push_back("mul");
    tmp.push_back("*");
    return tmp;
}
