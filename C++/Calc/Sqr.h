#if !defined SQR_H
#define SQR_H

#include <cmath>
#include "Oper.h"

class Sqr: public Operation
{
public:
    Sqr(); //A ctor that calls the base ctor.
    virtual double calc(double curr, std::vector<std::string> parameters);

private:
    std::vector<std::string> build_vec();
};
#endif
