#if !defined MUL_H
#define MUL_H

#include "Oper.h"

class Mul: public Operation
{
public:
    Mul(); //A ctor that calls the base ctor.
    virtual double calc(double curr, std::vector<std::string> parameters);

private:
    std::vector<std::string> build_vec();
};
#endif
