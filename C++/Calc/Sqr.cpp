#include "Sqr.h"

Sqr::Sqr() : Operation(build_vec(),1)
{}

double Sqr::calc(double curr, std::vector<std::string> parameters)
{
    if(parameters.size() != required)
    {
        std::cout << "Not supported!" <<std::endl;
    }
    return sqrt(curr);
}
std::vector<std::string> Sqr::build_vec()
{
    std::vector<std::string> tmp;
    tmp.push_back("sqrt");
    return tmp;
}
