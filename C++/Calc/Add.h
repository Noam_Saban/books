#if !defined ADD_H
#define ADD_H

#include "Oper.h"

class Add: public Operation
{
public:
    Add(); //A ctor that calls the base ctor.
    virtual double calc(double curr, std::vector<std::string> parameters);

private:
    std::vector<std::string> build_vec();
};
#endif
