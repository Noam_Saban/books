#if !defined SET_H
#define SET_H

#include "Oper.h"
class Set: public Operation
{
public:
    Set();
    virtual double calc(double curr, std::vector<std::string> parameters);
private:
    std::vector<std::string> build_vec();
};

#endif
