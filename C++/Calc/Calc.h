#if !defined CALC_H
#define CALC_H


/*
I took some liberty in this question.
Besides what is described in Operation.h ,
I have decided to parse the user input in a different way:
You suggested (Instructed?) to read a single "word", and by it (what operator does it match)
keep reading the number of needed parameters.
My way goes as such:
Read an entire line from stdin (std::cin), using a gnu function (which I have used in a past question I solved).
split that line into a vector of strings, by a static func of Operation.
Work with that vector from here on.

This way has a few advantages:
a. easier to implmenet (using the gnu func and c++ functionality).
b. more 'straight-forward' and reliable. meaning:
   in your way, the input:
        + 13 idk_what_else
    **should** not be too much of a problem,
    since you would parse just the first two words, and then the third would not match a single operator.

c. a bit more 'user-friendly' . put in whatever you want to do, press enter. done.
*/

#include "Add.h"
#include "Set.h"
#include "Sub.h"
#include "Mul.h"
#include "Div.h"
#include "Sqr.h"
#include "Pow.h"

class Calc
{
public:
    Calc();
    ~Calc();
    void run();

protected:
    //methods:
    std::vector<Operation*>::iterator find(std::vector<std::string> params);
    //searches the available operations for a suiting operation, and returns an iterator to it.

    //members:
    std::vector<Operation*> ops;
    double reg;
};

#endif
