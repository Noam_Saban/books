#if !defined FRAC_H
#define FRAC_H

#include <iostream>
#include <cmath>

#include "Exceptions.h"

class Fraction
{
public:
    //Ctors:
    Fraction(int numerator, int denomirator);
    Fraction(const Fraction& other);
    Fraction(float number);

    //"arithmatic" operators (for both another frack, and floats):
    Fraction operator+(const Fraction& other);
    Fraction operator+(float other);
    Fraction operator-(const Fraction& other);
    Fraction operator-(float other);
    Fraction operator*(const Fraction& other);
    Fraction operator*(float other);
    Fraction operator/(const Fraction& other);
    Fraction operator/(float other);
    Fraction& operator=(const Fraction& other);
    Fraction& operator=(float other);

    //conversion operators:
    operator float()const;
    operator int()const;

    int GetNumer()const;
    int GetDenom()const;
    void print()const;

protected:
    int numer;
    int denom;
};

#endif
