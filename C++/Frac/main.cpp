#include "Frac.h"

int main(void)
{
    Fraction a(2.5), b(4.5);
    a.print();
    b.print();
    a = a + b;
    a.print();

    Fraction c = a * b;
    c.print();
    c = c / a;
    c.print();

    try
    {
        //Fraction d (10,0); //throws an exception.
        Fraction e(-10);
        e.print();
        a = e - c;
        a.print();
        a = 0;
        a.print();

        a = 3;
        a.print();
    }
    catch(Invalid& e)
    {
        std::cout <<e.what() <<std::endl;
    }
    return 0;
}
