#include "Frac.h"

Fraction:: Fraction(int numerator, int denomirator) : numer(numerator), denom(denomirator)
{
    if (denom == 0)
    {
        throw Invalid();
    }
}
Fraction:: Fraction(const Fraction& other)
{
    numer = other.GetNumer();
    denom = other.GetDenom(); //There is an object, so we know these numbers are valid.
}
Fraction:: Fraction(float number)
{
    int i = 1;
    if(number !=0)
    {
        while(std::floor(number) != number)
        {
            i++;
            number *=10;
        }
        numer = number;
        denom  = std::pow(10, i -1);
    }
    else
    {
        numer = 0;
        denom = 1;
    }
}

Fraction Fraction::operator+(const Fraction& other)
{
    return Fraction ( (float(*this) + float(other)));
}
Fraction Fraction::operator+(float other)
{
    return Fraction( (float(*this) + other));
}

Fraction Fraction::operator-(const Fraction& other)
{
    return Fraction( (float(*this) - float(other)));
}
Fraction Fraction::operator-(float other)
{
    return Fraction( (float(*this) - other));
}

Fraction Fraction::operator*(const Fraction& other)
{
    return Fraction( (float(*this) * float(other)));
}
Fraction Fraction::operator*(float other)
{
    return Fraction( (float(*this) * other));
}

Fraction Fraction::operator/(const Fraction& other)
{
    return Fraction( (float(*this) / float(other)));
}
Fraction Fraction::operator/(float other)
{
    return Fraction( (float(*this) / other));
}

Fraction& Fraction::operator=(const Fraction& other)
{
    numer = other.GetNumer();
    denom = other.GetDenom(); //There is an object, so we know these numbers are valid.
    return *this;
}
Fraction& Fraction::operator=(float other)
{
    *this = Fraction(other);
    return *this;
}

Fraction:: operator float()const
{
    return ((float)numer/ (float)denom);
}
Fraction:: operator int()const
{
    return (numer/ denom);
}

int Fraction::GetNumer()const
{
    return numer;
}
int Fraction::GetDenom()const
{
    return denom;
}
void Fraction::print()const
{
    std::cout <<float(*this) <<std::endl;
}
