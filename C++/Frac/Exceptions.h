#if !defined EXCEPTION_H
#define EXCEPTION_H
 
#include <exception>

class Invalid: public std::exception
{
public:
    Invalid()
    {
        err = "Cannot divide by zero!" ;
    }
    virtual const char* what() const throw()
    {
        const char* foo = err.data();
        return foo;
    }
protected:
    std::string err;
};

#endif
