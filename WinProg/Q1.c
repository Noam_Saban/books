//If you get a weird linker error, read the note at Example.c


/*
This is exercise no.1 ("Open a window")
At page no 280.
The only difference between this code and the given example is in the function WndProc.
This program listens to two (well, actually three, including WM_DESTROY) messages:
WM_LBUTTONDOWN: a message notifying of a LEFT click,
WM_RBUTTONDOWN: a message notifying of a RIGHT click.

at a right click- update a counter,
at a left click- update the title of the window to the counter.
*/

#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NON_CONFORMING_SWPRINTFS

//These defines are necessary for _stprintf

#include <Windows.h>
#include <tchar.h>


#define BUF_SIZE 10


LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static int counter = 0;
	TCHAR  buffer[BUF_SIZE];
	
	switch (msg)
	{
	//Left click:
	case WM_LBUTTONDOWN:
		//convert an int to a string format that suits the SetWindowText func.
		_stprintf(buffer, _T("%d"), counter);
		SetWindowText(hWnd, buffer);
		break;

	//Right click:
	case WM_RBUTTONDOWN:
		counter++;
		break;

		case WM_DESTROY:
			//Breaks the message loop.
			PostQuitMessage(0);
			return 0;
	}
	//For the rest of the messages, call the default handlers.
	return DefWindowProc(hWnd, msg, wParam, lParam);
}



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR IpCmdLine, INT nCmdShow)
{
	HWND hWnd;
	WNDCLASS wc = { 0 };
	MSG msg = { 0 };


	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszClassName = TEXT("Foo"); //macro for handling unicode / ascii problems.
	wc.lpszMenuName = NULL;
	wc.hInstance = hInstance;

	if (!RegisterClass(&wc))
	{
		MessageBox(NULL, TEXT("Could not register."), TEXT("Error"), MB_OK | MB_ICONWARNING);
		return -1;
	}

	hWnd = CreateWindow(TEXT("Foo"),TEXT("Window"),WS_OVERLAPPEDWINDOW,
		100, 100,200, 300, NULL, NULL, hInstance, NULL);
	if (!hWnd)
	{
		MessageBox(NULL, TEXT("Could not create window"), TEXT("Error"), MB_OK | MB_ICONWARNING);
		return -1;
	}

	ShowWindow(hWnd, nCmdShow);

	while (GetMessage(&msg, NULL, 0,0))
	{ 
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	//The exit code received by WM_QUIT.
	return msg.wParam;
}