/*
This is exercise no.2 ("Goto a window") , at page no 280.
This program listens to WM_KEYDOWN messages, and checks if it was an enter key pressed.
If so, gets the local time and prints it to a log file.
If the file did not exist, it will create it.
This program uses GetTimeFormat() in order to parse the time struct into a proper buffer.
Well, it actually uses the function's ascii version, in order to avoid encoding problems (since the file is plain ascii).
*/
#include <Windows.h>

#define PATH "D://Users//user-pc//Desktop//WinProg//file.log"
//Change this to whatever directory you work with.
#define BUF_SIZE 100

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HANDLE hFile;
	OFSTRUCT  info ;
	SYSTEMTIME time;
	TCHAR buffer[BUF_SIZE];
	DWORD count;
	
	switch (msg)
	{
		case WM_KEYDOWN:
			//message type for key press.
			if (wParam == VK_RETURN)
			{
				//Enter virtual key.
				hFile = OpenFile(PATH, &info, OF_EXIST);
				if (hFile == HFILE_ERROR)
				{
					MessageBox(hWnd, TEXT("The file does not exist"), TEXT("Log"), MB_ICONERROR);
					hFile = OpenFile(PATH, &info, OF_CREATE);	
				}
				CloseHandle(hFile);

				hFile = OpenFile(PATH, &info,OF_WRITE);
				if (hFile == HFILE_ERROR)
				{
					MessageBox(hWnd, GetLastError(), TEXT("Error"),  MB_ICONERROR);
					CloseHandle(hFile);
					return -1;
				}

				//Even though WriteFile claims to write at THE END,
				//Without explicitly setting the cursor to the end of the file, the function would override existing data.
				SetFilePointer(hFile, 0, NULL, FILE_END);

				GetLocalTime(&time);
				GetTimeFormatA(0, LOCALE_NOUSEROVERRIDE, &time, NULL, buffer, BUF_SIZE);
				//To avoid encoding problems, I just used the ascii version of the function.
				//That is because the file itself is in plain ascii, and not in unicode.
				
				WriteFile(hFile, buffer, strlen(buffer), &count, NULL);
				WriteFile(hFile, "\n\r", 1, &count, NULL);

				CloseHandle(hFile);
			}
			break;

		case WM_DESTROY:
			//Breaks the message loop.
			PostQuitMessage(0);
			return 0;
	}
	//For the rest of the messages, call the default handlers.
	return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR IpCmdLine, INT nCmdShow)
{
	HWND hWnd;
	WNDCLASS wc = { 0 };
	MSG msg = { 0 };

	//Initialize the window struct.
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszClassName = TEXT("Foo"); //macro for handling unicode / ascii problems.
	wc.lpszMenuName = NULL;
	wc.hInstance = hInstance;

	if (!RegisterClass(&wc))
	{
		MessageBox(NULL, TEXT("Could not register."), TEXT("Error"), MB_OK | MB_ICONWARNING);
		return -1;
	}

	hWnd = CreateWindow(TEXT("Foo"),TEXT("Window"),WS_OVERLAPPEDWINDOW,
		100, 100,200, 300, NULL, NULL, hInstance, NULL);
	if (!hWnd)
	{
		MessageBox(NULL, TEXT("Could not create window"), TEXT("Error"), MB_OK | MB_ICONWARNING);
		return -1;
	}

	ShowWindow(hWnd, nCmdShow);

	//create the message pump.
	while (GetMessage(&msg, NULL, 0,0))
	{ 
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	//The exit code received by WM_QUIT.
	return msg.wParam;
}