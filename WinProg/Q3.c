/*
This is exercise no.3 ("Put a candle") , at page no 280.
This program gets the directory in which the operating system is installed.
The function is GetWindowsDirectory().
*/
#include <Windows.h>

#define BUF_SIZE 100

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//Just the default message. We don't to listen to any special messages.
	switch (msg)
	{
		case WM_DESTROY:
			//Breaks the message loop.
			PostQuitMessage(0);
			return 0;
	}
	//For the rest of the messages, call the default handlers.
	return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR IpCmdLine, INT nCmdShow)
{
	HWND hWnd;
	WNDCLASS wc = { 0 };
	MSG msg = { 0 };
	TCHAR buffer[BUF_SIZE];

	//Initialize the window struct.
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszClassName = TEXT("Foo"); //macro for handling unicode / ascii problems.
	wc.lpszMenuName = NULL;
	wc.hInstance = hInstance;

	if (!RegisterClass(&wc))
	{
		MessageBox(NULL, TEXT("Could not register."), TEXT("Error"), MB_OK | MB_ICONWARNING);
		return -1;
	}

	hWnd = CreateWindow(TEXT("Foo"),TEXT("Window"),WS_OVERLAPPEDWINDOW,
		100, 100,200, 300, NULL, NULL, hInstance, NULL);
		//100,100 is the position of the window
		//200,200 is the size.
	if (!hWnd)
	{
		MessageBox(NULL, TEXT("Could not create window"), TEXT("Error"), MB_OK | MB_ICONWARNING);
		return -1;
	}

	ShowWindow(hWnd, nCmdShow);
	GetWindowsDirectory(buffer, BUF_SIZE);
	MessageBox(hWnd, buffer, TEXT("Candle"), MB_OK);
	//No MB_ICONWARNING because this is not a warning.

	//create the message pump.
	while (GetMessage(&msg, NULL, 0,0))
	{ 
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	//The exit code received by WM_QUIT.
	return msg.wParam;
}