#include <Windows.h>
/*
if you get this error
unresolved external symbol _main referenced in function ___tmainCRTStartup
Go to: Project -> Properties -> Configuration Properties -> Linker -> System
and jist delete the subSystem line.
More info:
http://stackoverflow.com/questions/4845410/error-lnk2019-unresolved-external-symbol-main-referenced-in-function-tmainc

*/

/*
This is the example given in the books.
A basic WinMain program, that creates everything needed for a winprogram.
The WndProc func sends a message to the mine sweeper program at every mouse move on its (the program's) window.
So, you need to open the mine sweeper before running the program, and click on it once so the time will start running.

There is some error with the encoding of literal strings, so  I use the macro TEXT() .
*/
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//Messing with the mine sweeper
	HWND hWinmineWnd = NULL;

	switch (msg)
	{
		case WM_MOUSEMOVE:
			hWinmineWnd = FindWindow(NULL, TEXT("Minesweeper"));
			if (!hWinmineWnd)
			{
				MessageBox(hWnd, TEXT("Can't find the mine sweeper"), TEXT("Error"), MB_OK | MB_ICONWARNING);
				return 0;
			}
			PostMessage(hWinmineWnd, WM_TIMER, 0, 0);
			return 0;
	
		case WM_DESTROY:
			//Breaks the message loop.
			PostQuitMessage(0);
			return 0;
	}
	//For the rest of the messages, call the default handlers.
	return DefWindowProc(hWnd, msg, wParam, lParam);
}



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR IpCmdLine, INT nCmdShow)
{
	HWND hWnd;
	WNDCLASS wc = { 0 };
	MSG msg = { 0 };


	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszClassName = TEXT("Foo"); //macro for handling unicode / ascii problems.
	wc.lpszMenuName = NULL;
	wc.hInstance = hInstance;

	if (!RegisterClass(&wc))
	{
		MessageBox(NULL, TEXT("Could not register."), TEXT("Error"), MB_OK | MB_ICONWARNING);
		return -1;
	}

	hWnd = CreateWindow(TEXT("Foo"),TEXT("Window"),WS_OVERLAPPEDWINDOW,
		0, 0,200, 200, NULL, NULL, hInstance, NULL);
	if (!hWnd)
	{
		MessageBox(NULL, TEXT("Could not create window"), TEXT("Error"), MB_OK | MB_ICONWARNING);
		return -1;
	}

	ShowWindow(hWnd, nCmdShow);

	while (GetMessage(&msg, NULL, 0,0))
	{ 
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	//The exit code received by WM_QUIT.
	return msg.wParam;
}